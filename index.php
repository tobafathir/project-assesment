<?php
include 'connection.php';
include 'search.php';


$siswa=$db->query("select * from ff ");
$siswa->execute();
$data_siswa=$siswa->fetchAll();

$tampung=[];
if(isset($_POST['cari']))
{
    $tampung=searching($_POST['pilih']);
}
if(!empty($tampung))
{
    $data_siswa=$tampung;
}


if(isset($_POST['search']))
{
    $filter=$_POST['search'];
    $search=$db->prepare("select * from ff where path=? or senjata=? or efek=? or jarak=?"); // PDO statement
    $search->bindValue(1,$filter,PDO::PARAM_STR);
    $search->bindValue(2,$filter,PDO::PARAM_STR);
    $search->bindValue(3,$filter,PDO::PARAM_STR);
    $search->bindValue(4,$filter,PDO::PARAM_STR);
    $search->execute();     //Execution of PDO statement

    $tampil_data=$search->fetchAll(); //Result from PDO statement
    $row=$search->rowCount(); //Result from PDO statement
    
}

?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Skin Senjata Free Fire</title>
    <!-- <link rel="shortcut icon" href="gaming.png" type="image/x-icon"> -->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <link rel="shortcut icon" href="image/gar.png" type="image/x-icon">
    <style>
        #featured-post{
	        background-image: url("image/hayato.png");
            background-repeat: no-repeat;
	        background-size: cover;
	        background-position: center;
	        padding: 100px;
        }
        body{
            background: black;
        }
        #nav{
            position: fixed;
            top : 0;
            left :0;
            right :0;
        }
        #hr {
            background : grey;
        }
        
    </style>

</head>  
<body>

<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="nav" style="z-index: 1;">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-3">
                <button type="button" class="btn btn-outline-primary mx-auto" data-toggle="modal" data-target="#exampleModal">
                    <i class="fas fa-plus-circle"></i>
                </button>
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="character.html">Character<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="map.html">Map<span class="sr-only">(current)</span></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-3 col-md-9 collapse navbar-collapse" id="navbarSupportedContent">
        <form class="form-inline my-2 my-lg-0" action="index.php" method="POST">
            <input class="form-control mr-sm-2 bg-transparent text-light" autocomplete="off" type="search" name="pilih" placeholder="Cari Senjata" autofocus>
            <button type="submit" name="cari"  class="btn btn-outline-secondary">
                <i class="fas fa-search"></i>
            </button>
        </form>
    </div>
</nav>

<!-- home -->
<div id="featured-post" class="col-md-12 col-lg-12 col-sm-12">
    <div class="wrapper">
        <br>
        <br>
		<a href="#" class="category"></a>
		<h2 class="text-light pl-100">Official Free Fire</h2>
		<p class="text-light pl-100" >     KW      </p>
        <div class="date text-light">30 October 2020</div>
        <br>
        <br>
    </div>
    <img class="d-flex mx-auto col-lg-6 mt-1" src="image/free.png" alt="">
    <br>
    <br>
    <br>
    <br>
</div>
<br>
<br>
<br>
<br>
<br>
<br>

<!-- Card Title -->

<div class="container">
    <div class="row">
        <table class="table table-striped">
            <tbody>
                <?php foreach ($data_siswa as $key):?>
                    <div class="col-lg-3 col-md-12 p-0">
                        <div class="card m-2 border border-info bg-transparent" id="gambar" style="width: 18px, height: 50px; ">
                            <img src="<?php echo $key['path']; ?>" class="card-img-top">   
                            <div class="card-body border border-info">
                                <ul style="list-style: none;">
                                    <li>
                                        <b class="text-light">Nama : <?php echo $key['senjata'];?></b>
                                    </li>
                                    <br>
                                    <li>
                                        <b class="text-light">Efek Unggulan : <?php echo $key['efek'];?></b>
                                    </li>
                                    <br>
                                    <li>
                                        <b class="text-light">Target  : <?php echo $key['jarak'];?></b>
                                    </li>                                
                                    <br>
                                    <a type="button" class="btn btn-outline-danger" href="delete.php?id=<?php echo $key['id']; ?>"><i class="fas fa-trash-alt"></i></a>
                                    <a class="btn btn-outline-warning" href="edit.php?id=<?php echo $key['id']; ?>"><i class="fas fa-tools"></i></a>
                                </ul>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

      
<br>
<br>
<br>
<br>
<br>  
<br>
<br>
<br>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Masukan Jenis Senjata</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 ">
                            <form action="input.php" method="POST"  enctype="multipart/form-data">
                                <div class="form-group">
                                    <input required type="file" name="fileUpload">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Senjata</label>
                                    <input type="text" autocomplete="off" name="senjata" required class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Efek Unggulan</label>
                                    <input type="text" autocomplete="off" name="efek" required class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Jarak Senjata</label>
                                    <input type="text" autocomplete="off" name="jarak" required class="form-control">
                                </div>                    
                                <button type="submit" name="kirim" class="btn btn-success"><i class="fas fa-save"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-window-close"></i></button>
            </div>
        </div>
    </div>
</div>

<!-- Section Info -->
<section class="container-fluid">
    <hr id="hr">
        <div class="row mt-5 " id="back">
            <div class="mx-auto">
                <a class="fab fa-whatsapp text-decoration-none" style="font-size: 2rem; color:lightgreen;" href="https://wa.me/6289613726098" class="text-light text-decoration-none align-top mt-3 ml-2" target="_blank"> | </a>
                <a class="fab  fa-facebook text-decoration-none" style="font-size: 2rem; color: blue;" href="https://web.facebook.com/auladya.sopo/" class="text-light text-decoration-none align-top mt-3 ml-2"> | </a>
                <a class="far fa-envelope text-decoration-none" style="font-size: 2rem; color: red;" href="mailto:tobafathir0404@gmail.com " class="text-light  text-decoration-none align-top mt-3 ml-2" > | </a>
                <a class="fab fa-github text-decoration-none" style="font-size: 2rem; color: white; " href="https://github.com/tobafathir"  class="text-decoration-none align-top mt-3 ml-2;"></a>
                <p class="text-light text-center">&copy; Toba Fathir</p>
            </div>
        </div>
    <hr id="hr">
</section>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script>
    $('.modalPop').click(function(el) {
            el.preventDefault();
            $('.modal-title').html($(this).html());
            action = $(this).html();

            if (action == 'View') {
                $('#modalPop').modal('show');
                img_src = $(this).parent().siblings('img').attr('src');
                html_img = "<img src=" + img_src + " style='width:100%;height:100%;'>";
                $('.modal-body').html(html_img);
            } else if (action == 'Change') {
                $('.modal-body').html("");
                $('#modalPop').modal('show');

                template=`
                    <form action="index.php" method="POST">
                        <input type="hidden" name="id">
                        <textarea class="form-control" name="caption"></textarea>
                        <input class="form-control" type="submit" name="action" value="Update">
                    </form>
                `;
            }
        });
    </script>
    <script>
        $('.link-smooth').click(function(e){
            
            if (this.hash !== "") {
               // Prevent default anchor click behavior
               event.preventDefault();

               // Store hash
               var hash = this.hash;

               // Using jQuery's animate() method to add smooth page scroll
               // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
               $('html, body').animate({
                   scrollTop: $(hash).offset().top
               }, 800, function () {

                   // Add hash (#) to URL when done scrolling (default click behavior)
                   window.location.hash = hash;
               });
           } // End if
       });
    </script>
  </body>
</html>