<?php

include "connection.php";
$daftar=$db->query("select * from ff where id=".$_GET['id']);
$data_daftar=$daftar->fetchAll();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <title>Form Edit Senjata</title>
    <style>
        body{
            background : url(image/ses.jpg) no-repeat fixed;
            background-size : 1550px;
        }
        #form{
            background-color:grey;
            opacity:0.7;
            filter:alpha(opacity=60);

            position : fixed;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row vh-100 justify-content-center col-lg-12 col-sm-6 col-md-12" >
        <div class="col-5 border border-secondary rounded p-3 align-self-center" id="form">
            <h2 class="text-center">Edit Senjata</h2>
            <form action="update.php" method="POST">
                <input type="hidden" name="id"  value="<?php echo $data_daftar[0]['id']; ?>">
                <div class="form-group">
                    <label for="exampleInputEmail1 text-light">Nama</label>
                    <input type="text"  required name="senjata" value="<?php echo $data_daftar[0]['senjata']; ?>" class="form-control bg-transparent text-light">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1 text-light">Efek</label>
                    <input type="text" required name="efek" value="<?php echo $data_daftar[0]['efek']; ?>" class="form-control bg-transparent text-light">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Target</label>
                    <input type="text" required class="form-control bg-transparent text-light" name="jarak" value="<?php echo $data_daftar[0]['jarak']; ?>" >
                </div>
                <button type="submit" class="btn btn-outline-primary text-light">Simpan</button>
            </form>
        </div>
    </div>
</div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>'