<?php

include "connection.php";
    
if (isset($_POST['kirim'])) {
    $upload = uploadFiles($_FILES);    

    if (is_array($upload)) {

        $save_path = $db->prepare("insert into ff(path,senjata,efek,jarak) values(?,?,?,?)");


        $save_path->bindParam(1, $upload[1], PDO::PARAM_STR);
        $save_path->bindParam(2, $_POST['senjata'], PDO::PARAM_STR);
        $save_path->bindParam(3, $_POST['efek'], PDO::PARAM_STR);
        $save_path->bindParam(4, $_POST['jarak'], PDO::PARAM_STR);


        if ($save_path->execute()) {
            $message = $upload[0];
            header('location:index.php');
        }
    } else {
        $message = $upload;
    }
}

function uploadFiles($files)
{

    $target_dir = "image/";
    $user_file = $target_dir . basename($files['fileUpload']['name']);

    $fileType = strtolower(pathinfo($user_file, PATHINFO_EXTENSION));

    $allowType = ['jpg', 'jpeg', 'png','gif'];

    if (!in_array($fileType, $allowType)) {
        return "File not allowed";
    }

    $check = getimagesize($files["fileUpload"]["tmp_name"]);

    if (!$check) {
        return "File is not image";
    }

    if ($files["fileUpload"]["size"] > 1000000) {
        return "File is larger than 1mb";
    }

    $temp_name = $target_dir . 'upload_' . date('d-m-Y_His') . "." . $fileType;

    if (!move_uploaded_file($files["fileUpload"]["tmp_name"], $temp_name)) {

        return "File not uploaded";
    }

    return ['file success uploaded', $temp_name];
    header('location: index.php');
}

?>
